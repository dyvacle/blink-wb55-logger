/**
  ******************************************************************************
  * @file           : logger.c
  * @brief          : logger tools
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/

// project
#include "logger.h"
#include "printf.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

// extern void Error_Handler(void);

/* Private variables ---------------------------------------------------------*/

#ifdef DEBUG

// global of the module,
// the default output value is to UART for development
// in autonomous mode the default value should be SD
dbg_out_t 		dbg_out = DBG_UART;

// for message, variables used in macros
char 			dbg_tag[TXT_TAG_SIZE] = "no tag";
char 			dbg_msg[TXT_OUT_SIZE] = "no msg";
logger_obj_t 	dbg_obj = {0, DBG_INFO, "no object"};
osStatus_t 		rstatus = osOK;

// for SD
// todo

/* Private user code ---------------------------------------------------------*/

/**
  * @brief  logger initialization
  * @param  none
  * @retval none
 */
void logger_init(void) {

	// todo read from eeprom the value of output

	switch (dbg_out) {

	case DBG_UART:
		// nothing special
		break;

	case DBG_SD:
		// mount SD card here ?
		// define file for output
		// check if it is possible to write in
		// todo
		break;
	}

}

/**
  * @brief  logger managing
  * @param  none
  * @retval none
 */
void logger(void)
{
	logger_obj_t	dbg_in;
	uint8_t			dbg_prio;
	uint32_t 		dbg_count;
	char			dbg_txt[TXT_TAG_SIZE+TXT_OUT_SIZE];

	osStatus_t rstatus = osMessageQueueGet(logQueueHandle, &dbg_in, &dbg_prio, osWaitForever);

	if (rstatus == osOK) {

		switch (dbg_out) {

		case DBG_UART:
			dbg_count = osMessageQueueGetCount(logQueueHandle);
			snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "{%lu|%lu} %s\r\n", dbg_count, dbg_in.id, dbg_in.txt);

			osSemaphoreAcquire(uartSemHandle, 0);
				HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *) dbg_txt, strlen(dbg_txt), strlen(dbg_txt)*OUTPUT_TIMEOUT);
			osSemaphoreRelease(uartSemHandle);

			break;

		case DBG_SD:
			dbg_count = osMessageQueueGetCount(logQueueHandle);
			snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "{%lu|%lu} %s\r\n", dbg_count, dbg_in.id, dbg_in.txt);

			osSemaphoreAcquire(sdSemHandle, 0);
				// todo put msg in file fprintf( sd_log_file, dbg_txt);
			osSemaphoreRelease(sdSemHandle);

			rstatus = osErrorParameter;
			break;

		}

		// control if dbg_count arrives near 16 the limit of the queue
		if (dbg_count >= (MAX_ITEMS*0.8)) {
			if (dbg_count == MAX_ITEMS) {
				snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "error queue\r\n");
			} else if (dbg_count >= (MAX_ITEMS*0.9)) {
				snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "critical queue\r\n");
			} else {
				snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "dangerous queue\r\n");
			}
			osSemaphoreAcquire(uartSemHandle, 0);
				HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *) dbg_txt, strlen(dbg_txt), strlen(dbg_txt)*OUTPUT_TIMEOUT);
			osSemaphoreRelease(uartSemHandle);

			if (dbg_count == MAX_ITEMS) Error_Handler();
		}

	} else {
		// rstatus value to control when not ok
		snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "error msg queue get\r\n");
		osSemaphoreAcquire(uartSemHandle, 0);
			HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *) dbg_txt, strlen(dbg_txt), strlen(dbg_txt)*OUTPUT_TIMEOUT);
		osSemaphoreRelease(uartSemHandle);
		// todo
		Error_Handler();
	}
}

// redefines Error_Handler

void _Error_Handler(char * file, int line)
{
	char dbg_txt[TXT_TAG_SIZE+TXT_OUT_SIZE];

	HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, SET);
    snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "Error reported... File %s on line %d\n\r", file, line);
    HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t*) dbg_txt, strlen(dbg_txt), strlen(dbg_txt)*OUTPUT_TIMEOUT);
}

#endif


// redefines output input standard io

// extern HAL_StatusTypeDef HAL_UART_Transmit(
// 	UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

// extern HAL_StatusTypeDef HAL_UART_Receive(
// 	UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

#ifdef __GNUC__

int __io_putchar(int ch)
{
   uint8_t ch8 = ch;
   HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *)&ch8, 1, OUTPUT_TIMEOUT);
   return ch;
}

int __io_getchar()
{
   uint8_t ch8;
   HAL_UART_Receive(&DEBUG_HANDLER, &ch8, 1, INPUT_TIMEOUT);
   return (int)ch8;
}

#else

int fputc(int ch, FILE *f)
{
  HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *)&ch, 1, OUTPUT_TIMEOUT);
  return ch;
}

int __io_getchar()
{
   uint8_t ch8;
   HAL_UART_Receive(&DEBUG_HANDLER, &ch8, 1, INPUT_TIMEOUT);
   return (int)ch8;
}

#endif /* __GNUC__ */
