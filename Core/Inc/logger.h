/**
  ******************************************************************************
  * @file           : logger.h
  * @brief          : Header for logger.c file.
  *
  * This file contains the debug tools.
  *
  * Define DEBUG or not to have trace or not
  * See DEBUG_HANDLER to define the uart board to use
  *
  * use debug macros for trace
  * dbg_printf classic way
  * dbg_trace with ticks tag
  * dbg_log with file name + line tag
  *
  * Add __weak to Error_Handler in main.c
  * To be able to use %f in format printf, need also -u _printf_float @ linker
  *
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LOGGER_H
#define __LOGGER_H

/* Private includes ----------------------------------------------------------*/

// HAL
#include "stm32wbxx_hal.h"
#include "stm32wbxx_ll_system.h"

// OS
#include "cmsis_os2.h"

// project

#include "printf.h"

// use common libs
#include <stdbool.h>	// bool
#include <string.h>		// strlen...

/* Overload the Error_Handler STM32 function ---------------------------------*/

#define GET_MACRO( _0, _1, NAME, ... ) NAME
#define Error_Handler(...) GET_MACRO( _0, ##__VA_ARGS__, Error_Handler1, Error_Handler0 )()
#define Error_Handler0() _Error_Handler( __FILE__, __LINE__ )
#define Error_Handler1(unused) _Error_Handler( char * file, int line )
void 	_Error_Handler(char *, int);

/* Exported constants --------------------------------------------------------*/

#define MAX_ITEMS 			16 /* nb max of items into logger Queue (see ioc) */

#define DEBUG_HANDLER		huart1
extern UART_HandleTypeDef	DEBUG_HANDLER; // serial monitor

#ifndef DEBUG /* to avoid warnings */
#define DEBUG /*to be defined or to be in comment*/
#endif /* to avoid warnings */

#define TXT_IN_SIZE			255  /* max size buffer in */
#define TXT_TAG_SIZE		55   /* max size tag */
#define TXT_OUT_SIZE		200  /* max size buffer out */
#define INPUT_EOT			'\r' /* Carriage return */
#define INPUT_TIMEOUT		10   /* 10 ms short time out for char input */
#define OUTPUT_TIMEOUT		10   /* 10 ms time out for char output */

/* Exported types ------------------------------------------------------------*/

typedef enum {
	DBG_INFO,
	DBG_WARNING,
	DBG_ERROR
} dbg_lvl_t; // todo

// output
typedef enum {
	DBG_UART,
	DBG_SD,
} dbg_out_t; // todo

// object data type in queue
typedef struct {
	uint32_t	id;
	dbg_lvl_t	lvl; // todo
	char 		txt[TXT_TAG_SIZE+TXT_OUT_SIZE];
	// TXT_TAG_SIZE+TXT_OUT_SIZE = max 255 because of using index on unit8_t
} logger_obj_t;

/* Exported macro ------------------------------------------------------------*/

#ifdef DEBUG /* DeBUGGING MODE */

// global of the module
extern dbg_out_t 			dbg_out;

// for message
extern char 				dbg_tag[TXT_TAG_SIZE];
extern char 				dbg_msg[TXT_OUT_SIZE];
extern logger_obj_t 		dbg_obj;
extern osStatus_t 			rstatus;

extern osSemaphoreId_t 		loggerSemHandle;
extern osSemaphoreId_t 		uartSemHandle;
extern osSemaphoreId_t 		sdSemHandle;
extern osMessageQueueId_t 	logQueueHandle;

/**
 * Note for the macro using osMessageQueuePut(logQueueHandle, &dbg_obj, 0, 0);
 *
 * osOK: the message has been put into the queue.
 * osErrorTimeout: the message could not be put into the queue in the given time (wait-timed semantics).
 * osErrorResource: not enough space in the queue (try semantics).
 * osErrorParameter: parameter mq_id is NULL or invalid, non-zero timeout specified in an ISR.
 */

/* macro to print debug on serial monitor with no tag */
#define dbg_printf(...) { \
	osSemaphoreAcquire(loggerSemHandle, 0); \
		snprintf(dbg_msg, TXT_OUT_SIZE, __VA_ARGS__); /* args */ \
		HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *) dbg_msg, strlen(dbg_msg), strlen(dbg_msg)*OUTPUT_TIMEOUT); \
	osSemaphoreRelease(loggerSemHandle); \
} /* end debug macro */

/* macro to trace debug on serial monitor or SD with ticks tag */
#define dbg_trace(...) { \
	osSemaphoreAcquire(loggerSemHandle, 0); \
		snprintf(dbg_tag, TXT_TAG_SIZE, "<%lu>", HAL_GetTick());  /* ticks tag */ \
		snprintf(dbg_msg, TXT_OUT_SIZE, __VA_ARGS__); /* args */ \
		snprintf(dbg_obj.txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "%s %s", dbg_tag, dbg_msg); /* concat */ \
		dbg_obj.id += 1; \
		rstatus = osMessageQueuePut(logQueueHandle, &dbg_obj, dbg_obj.lvl, 100); \
		if (rstatus != osOK) { \
			dbg_printf("dbg_trace error %d put in queue", rstatus); \
		} \
	osSemaphoreRelease(loggerSemHandle); \
} /* end debug macro */

/* to have file name without path */
// #define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

/* macro to trace debug on serial monitor or SD with file & line number tag */
#define dbg_log(...) { \
	osSemaphoreAcquire(loggerSemHandle, 0); \
		snprintf(dbg_tag, TXT_TAG_SIZE, "%s [%d]", __FILENAME__, __LINE__);  /* file line tag */ \
		snprintf(dbg_msg, TXT_OUT_SIZE, __VA_ARGS__); /* args */ \
		snprintf(dbg_obj.txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "%s %s", dbg_tag, dbg_msg); /* concat */ \
		dbg_obj.id += 1; \
		rstatus = osMessageQueuePut(logQueueHandle, &dbg_obj, dbg_obj.lvl, 100); \
		if (rstatus != osOK) { \
			dbg_printf("dbg_log error %d put in queue", rstatus); \
		} \
	osSemaphoreRelease(loggerSemHandle); \
} /* end debug macro */


#else /* NO DEBUG */

/* macro are NOP */
#define dbg_trace(...) {}
#define dbg_log(...) {}

#endif /* DEBUG */

/* Exported functions prototypes ---------------------------------------------*/

// to be set in function logger_fct for thread loggerHandle

#ifdef DEBUG

/**
  * @brief  logger initialization
  * @param  none
  * @retval none
 */
void logger_init(void);

/**
  * @brief  logger managing
  * @param  none
  * @retval none
 */
void logger(void);

#endif /* DEBUG */

/* Private defines -----------------------------------------------------------*/

/* Redefines the weak functions :

#ifdef __GNUC__
int __io_putchar(int ch);
int __io_getchar();
#else
int fputc(int ch, FILE *f);
int __io_getchar();
#endif

*/

#endif /* __LOGGER_H */

